# frozen_string_literal: true

Rails.application.routes.draw do

  root 'welcome#index'

  get 'welcome/index'

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :guests, only: %i[create]
    end
  end

  # Redirect all unknown requests to welcome page.
  get '*path', to: 'welcome#index', via: :all
  
end
