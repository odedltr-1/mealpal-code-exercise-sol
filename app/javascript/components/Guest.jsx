import React from "react"

const Guest = () => {
  return (
    <div className="guest">
      <h1 className="guest__title">Welcome</h1>
      <p className="guest__subtitle">
        New York guidelines require us to collect your contact information. You
        will only be contacted if required by law.
      </p>
    </div>
  )
}

export default Guest
